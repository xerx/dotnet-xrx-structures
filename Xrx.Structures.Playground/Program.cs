﻿using Xrx.Structures.Tree;
using Xrx.Structures.Tree.Abstractions;

namespace Xrx.Structures.Playground
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            ITree tree = new PathTree("root");

            var intNode = new MapNode("root.node1.node1_1".ToPath())
            {
                Value = 2
            };
            var stringNode = new MapNode("root.node1.node1_2".ToPath())
            {
                Value = "Hi"
            };
            var intNode2 = new MapNode("root.node1.node1_3".ToPath())
            {
                Value = 3
            };
            tree.AddNode(intNode);
            tree.AddNode(stringNode);
            tree.AddNode(intNode2);

        }
    }
}
