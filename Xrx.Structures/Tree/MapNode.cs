﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Xrx.Structures.Tree.Abstractions;
using Xrx.Structures.Tree.Serializers;

namespace Xrx.Structures.Tree
{
    [JsonConverter(typeof(MapNodeSerializer))]
    public class MapNode : Dictionary<string, INode>, IParentNode
    {
        public string Name { get; protected set; }
        public object Value { get; set; }
        public Path Path { get; protected set; }

        private IParentNode parent;
        IParentNode INode.Parent
        {
            get => parent;
            set
            {
                parent = value;
                if (parent != null)
                {
                    Path = $"{parent.Path}.{Name}";
                    foreach (var pair in this)
                    {
                        pair.Value.Parent = this;
                    }
                }
                else
                {
                    Path = Name;
                }
            }
        }

        int INode.Depth => Path.Segments.Count();
        bool INode.IsLeaf => Count == 0;
        bool INode.IsRoot => (this as INode).Parent == null;
        int IParentNode.NumChildren => Count;

        public MapNode(string name)
        {
            Name = name;
            Path = name;
        }
        public MapNode(Path path)
        {
            Path = path;
            Name = Path.Segments.Last();
        }
        T INode.GetValue<T>() => (T)Value;

        bool IParentNode.AddChild(INode node)
        {
            if (string.IsNullOrEmpty(node.Name))
            {
                throw new ArgumentNullException(nameof(Name));
            }
            node.Parent = this;
            this[node.Name] = node;
            return true;
        }
        INode IParentNode.GetChild(string name)
        {
            try
            {
                return this[name];
            }
            catch (Exception)
            {
                throw new NullReferenceException("Child node not found");
            }
        }

        INode IParentNode.GetChild<T>(T value)
        {
            foreach (var pair in this)
            {
                if(Equals(value, pair.Value.Value))
                {
                    return pair.Value;
                }
            }
            return null;
        }

        bool IParentNode.RemoveChild(string name)
        {
            INode node = (this as IParentNode).GetChild(name);
            if (node != null)
            {
                node.Dispose();
                try
                {
                    return Remove(name);
                }
                catch (Exception)
                {
                    throw new NullReferenceException("Child node not removed");
                }
            }
            return false;
        }
        bool IParentNode.RemoveChild<T>(T value)
        {
            foreach (var pair in this)
            {
                if (Equals(value, pair.Value.Value))
                {
                    (this as IParentNode).RemoveChild(pair.Key);
                    return true;
                }
            }
            return false;
        }

        bool IParentNode.ContainsNode(string name)
        {
            if(string.IsNullOrEmpty(name))
            {
                return false;
            }
            return ContainsKey(name);
        }

        bool IParentNode.ContainsNode(INode node)
        {
            if (node == null)
            {
                return false;
            }
            return ContainsValue(node);
        }
        public virtual void Dispose()
        {
            parent = null;
            foreach (var node in Values)
            {
                node.Dispose();
            }
        }
    }
}
