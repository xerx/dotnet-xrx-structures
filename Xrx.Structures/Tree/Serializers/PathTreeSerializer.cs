﻿using Xrx.Structures.Tree.Abstractions;

namespace Xrx.Structures.Tree.Serializers
{
    public class PathTreeSerializer : MapNodeSerializer
    {
        protected override IParentNode CreateObject(string name)
        {
            return new PathTree(name);
        }
    }
}
