﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xrx.Structures.Tree.Abstractions;

namespace Xrx.Structures.Tree.Serializers
{
    public class MapNodeSerializer : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken token = JToken.Load(reader);
            string name = token.Value<string>("name");

            IParentNode node = CreateObject(name);
            node.Value = token.Value<object>("value");
            var children = token["children"]?.Children();
            if (children != null)
            {
                MapNode childNode;
                foreach (var child in children)
                {
                    childNode = child.ToObject<MapNode>();
                    node.AddChild(childNode);
                }
            }
            return node;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            MapNode mapNode = value as MapNode;
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(mapNode.Name);
            if (mapNode.Value != default)
            {
                writer.WritePropertyName("value");
                writer.WriteValue(mapNode.Value);
            }
            if (!(mapNode as INode).IsLeaf)
            {
                writer.WritePropertyName("children");
                writer.WriteStartArray();
                foreach (var node in mapNode)
                {
                    serializer.Serialize(writer, node.Value);
                }
                writer.WriteEndArray();
            }
            writer.WriteEndObject();
        }
        protected virtual IParentNode CreateObject(string name)
        {
            return new MapNode(name);
        }
        public override bool CanConvert(Type objectType)
        {
            return true;
        }
    }
}
