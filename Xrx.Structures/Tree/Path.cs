﻿using System.Collections.Generic;
using System.Linq;

namespace Xrx.Structures.Tree
{
    public struct Path
    {
        public static implicit operator Path(string path) => new Path { Value = path };

        public static implicit operator Path(List<string> segments) => new Path { Segments = segments };

        private string _value;
        public string Value
        {
            get
            {
                if (string.IsNullOrEmpty(_value))
                {
                    _value = string.Join(".", _segments);
                }
                return _value;
            }
            set => _value = value;
        }
        private string _parent;
        public string Parent
        {
            get
            {
                if (_parent == null)
                {
                    if (Segments.Count() > 1)
                    {
                        List<string> parent = Segments.ToList();
                        parent.RemoveAt(parent.Count - 1);
                        _parent = string.Join(".", parent);
                    }
                    else
                    {
                        _parent = "";
                    }
                }
                return _parent;
            }
        }
        private IEnumerable<string> _segments;
        public IEnumerable<string> Segments
        {
            get
            {
                if (_segments == null)
                {
                    _segments = _value.Split('.');
                }
                return _segments;
            }
            set => _segments = value;
        }
        public override string ToString() => Value ?? "";
    }
    public static class PathExtensions
    {
        public static Path ToPath(this string pathString) =>
            new Path { Value = pathString };
    }

}
