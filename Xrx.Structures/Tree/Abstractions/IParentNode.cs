﻿using System.Collections;

namespace Xrx.Structures.Tree.Abstractions
{
    public interface IParentNode : IEnumerable, INode
    {
        bool AddChild(INode node);
        bool RemoveChild(string name);
        bool RemoveChild<T>(T value);
        INode GetChild(string name);
        INode GetChild<T>(T value);
        bool ContainsNode(string name);
        bool ContainsNode(INode node);
        int NumChildren { get; }
    }
}
