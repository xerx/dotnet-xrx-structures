﻿namespace Xrx.Structures.Tree.Abstractions
{
    public interface ITree
    {
        bool AddNode(INode node);
        bool RemoveNode(string path);
        INode GetNode(string path);
    }
}
