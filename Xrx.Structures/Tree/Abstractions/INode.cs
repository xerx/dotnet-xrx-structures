﻿using System;
using Newtonsoft.Json;

namespace Xrx.Structures.Tree.Abstractions
{
    public interface INode : IDisposable
    {
        string Name { get; }
        Path Path { get; }
        object Value { get; set; }
        T GetValue<T>();
        IParentNode Parent { get; set; }
        bool IsLeaf { get; }
        bool IsRoot { get; }
        int Depth { get; }
    }
}