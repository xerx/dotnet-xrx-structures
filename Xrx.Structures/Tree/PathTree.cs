﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Xrx.Structures.Tree.Abstractions;
using Xrx.Structures.Tree.Serializers;

namespace Xrx.Structures.Tree
{
    [JsonConverter(typeof(PathTreeSerializer))]
    public class PathTree : MapNode, ITree
    {
        public PathTree(string name) : base(name)
        {
        }

        bool ITree.AddNode(INode node)
        {
            if (Name != node.Path.Segments.First())
            {
                throw new NullReferenceException("Root node not found");
            }
            IParentNode currentNode = this;
            IEnumerable<string> segments = node.Path.Segments;
            for (int i = 1; i < segments.Count() - 1; i++)
            {
                try
                {
                    currentNode = currentNode.GetChild(segments.ElementAt(i)) as IParentNode;
                }
                catch (NullReferenceException)
                {
                    MapNode newNode = new MapNode(segments.ElementAt(i));
                    currentNode.AddChild(newNode);
                    currentNode = newNode;
                }
            }
            return currentNode.AddChild(node);
        }

        INode ITree.GetNode(string path)
        {
            Path nodePath = path;
            if (Name != nodePath.Segments.First())
            {
                throw new NullReferenceException("Node not found");
            }
            if (nodePath.Segments.Count() == 1)
            {
                return this;
            }
            IParentNode node = this;
            IEnumerable<string> segments = nodePath.Segments;
            for (int i = 1; i < segments.Count(); i++)
            {
                node = node.GetChild(segments.ElementAtOrDefault(i)) as IParentNode;
            }
            return node;
        }
        bool ITree.RemoveNode(string path)
        {
            INode node = (this as ITree).GetNode(path);
            if (node.IsRoot)
            {
                throw new ArgumentException("Cannot remove root");
            }
            return node.Parent.RemoveChild(node.Name);
        }
    }
}
